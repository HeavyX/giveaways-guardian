const eventSource = new EventSource('http://localhost:5000/connect')

eventSource.addEventListener('keepAlive', (e) => {
	console.log("keepAlive");
}, false)

eventSource.addEventListener('actualiseHealthBar', (e) => {
	const data = JSON.parse(e.data)
	const damageDealt = data.damageDealt
	const dead = data.dead;
	const totalHealth = data.totalHealth;
	const actualHealth= data.actualHealth;

	console.log('data', data);

	if(dead) {
		document.getElementById('damageDealer').innerHTML = "MORT"
	} else {
		document.getElementById('damageDealer').innerHTML = actualHealth + ' / ' + totalHealth
		kick()
		displayDamage(damageDealt)
		shakkyShakky()
	}
	
}, false)

shakkyShakky = () => {
	document.getElementById('damageDealer').classList.add("shake")
	setTimeout(() => {
		document.getElementById('damageDealer').classList.remove("shake")
	}, 500)
}

kick = () => {
	document.getElementById('gif').src = './kick.gif'
	setTimeout(() => {
		document.getElementById('gif').src = './idle.gif'
	}, 1000)
}

displayDamage = (damageDealt) => {
	document.getElementById('displayDamage').innerHTML = '-' + damageDealt
	document.getElementById('displayDamage').classList.add("fadeIn")

	setTimeout(() => {
		document.getElementById('displayDamage').classList.remove("fadeIn")
		document.getElementById('displayDamage').classList.add("slideOutUp")
	}, 300)

	setTimeout(() => {
		document.getElementById('displayDamage').classList.remove("slideOutUp")
		document.getElementById('displayDamage').innerHTML = ''
	}, 1000)
}