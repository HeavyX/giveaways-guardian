import * as fs from 'fs'

if (!fs.existsSync('./src/db')){
	fs.mkdirSync('./src/db')
}

// needed to place here, so we can create the db folder before
// trying to create any db file
import { ChatHandler } from './src/handlers/twitch-chat/chat.handler'

const opts = {
	identity: {
		username: 'captainheavyx',
		password: 'oauth:xzn1fznnkfuyuji85dvc5o32w9420a'
	},
	channels: [
		'iheavyx'
	]
}

const connection = new ChatHandler(opts)

connection.connect()
