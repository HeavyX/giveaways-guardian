const express = require('express')
const request = require('request')
const EventEmitter = require('events')

const app = express()
const Stream = new EventEmitter()

app.use(express.json())
app.use(express.static('./health-bar'))

app.post('/sendEvent', (req, res) => {
	Stream.emit("push", "actualiseHealthBar", req.body);
});

app.get('/connect', (req, res) => {
	res.writeHead(200, {
    'Content-Type': 'text/event-stream',
    'Cache-Control': 'no-cache',
    'Connection': 'keep-alive'
	})

	setInterval(() => {
		res.write("event: keepAlive \n" + "data: keepAlive \n\n");
	}, 20000)

	Stream.on("push", (event, data) => {
    res.write("event: " + String(event) + "\n" + "data: " + JSON.stringify(data) + "\n\n");
  })
})

const server = app.listen(5000, () => {
	const host = server.address().address
	const port = server.address().port
	console.log('Listening at http://%s:%s', host, port)
})