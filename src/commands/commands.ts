import { GuardianDAO } from '../dao/guardian.dao';
import { UserDAO } from '../dao/user.dao';
import { SSEService } from '../services/sse.service';
import COMMANDS from '../config/commands';
import { User } from '../models/user.model';
import { Guardian } from '../models/guardian.model';

const guardianDAO = new GuardianDAO()
const userDAO = new UserDAO()
const sse = new SSEService()

export class Commands {
	kick(user: User, guardian: Guardian, client, target) {
		if(guardianDAO.isAlreadyParticipant(user)) {
			if(user.canUseCommand('kick', 5)) {
				user.updateUserLastUsage('kick')
				if(!guardian.isDead()) {
					const damageDealt = user.rollDice()
					guardian.removeHealth(damageDealt, user)
					sse.updateHealthBar(guardian.health, guardian.currentHealth, damageDealt)
					user.addParticipation(guardian)
					if(guardian.currentHealth <= 0) {
						// Reset all user kick, delete them all
						sse.updateHealthBar(guardian.health, 0, 0, true)
						client.say(target, `${user.displayName} à porté le coup final à ${guardian.name} !!! On va enfin avoir le giveaway !!`)
					}
				} else {
					client.say(target, COMMANDS.kick.dead)
				}
			} else {
				const remainingTime = user.getRemainingTime('kick')
				client.say(target, `Tu dois encore attendre ${remainingTime} avant de pouvoir frapper le gardien à nouveau ${user.displayName} !!`)
			}
		}
	}

	guardian(guardian, client, target) {
		if(userDAO.canUseBotCommand('guardian', 2)) {
			userDAO.updateBotLastUsage('guardian')
			client.say(target, COMMANDS.guardian.fighting + " " + guardian.name + " " + COMMANDS.guardian.stillHas + " " + guardian.currentHealth + " " + COMMANDS.guardian.healthPoints)
		}
	}

	bitch(client, target) {
		if(userDAO.canUseBotCommand('bitch', 2)) {
			userDAO.updateBotLastUsage('bitch')
			const answers = COMMANDS.bitch
			client.say(target, answers[Math.floor(Math.random() * answers.length)])
		}
	}

	giveaway(guardian, client, target) {
		const winner = guardian.giveaway()
		if(winner == null) { // In case you decide to kill the bot alone and nobody hit
			client.say(target, COMMANDS.giveaway.noWinner)
		} else if(guardian.isDead()) {
			client.say(target, COMMANDS.giveaway.winnerIs + " " + winner.user + " " + COMMANDS.giveaway.congratulation)
		} else {
			client.say(target, COMMANDS.giveaway.notDefeated)
		}
	}

	win(commandParams, guardian, client, target) {
		const damageDealt = commandParams[0]
		const lastGuardianHealth = guardian.currentHealth
		guardian.removeHealthWin(damageDealt)
		sse.updateHealthBar(guardian.health, guardian.currentHealth)
		if(guardian.currentHealth <= 0) {
			client.say(target, `IHeavyX à porté le coup final à ${guardian.name} en remportant la partie précédente !!! On va enfin avoir le giveaway !!`)
		} else {
			client.say(target, `Le Gardien avait ${lastGuardianHealth} points de vie, grâce à son skill immense IHeavyX à remporté la partie précédente et lui inflige ${damageDealt} points de dégats, le Gardien à désormais ${guardian.currentHealth} points de vie`)
		}
	}

	stats(user, guardian, client, target) {
		if(user.canUseCommand('stats', 2)) {
			if(guardianDAO.isAlreadyParticipant(user)) {
				user.updateUserLastUsage('stats')
				const stats = guardian.getUserStats(user)
				const coup = (stats.numberOfHits > 1) ? COMMANDS.stats.hit : COMMANDS.stats.hits
				client.say(target, `Dans ce combat acharné contre ${guardian.name} tu lui as infligé ${stats.damageDealt} en ${stats.numberOfHits} ${coup} !`)
			} else {
				client.say(target, `Tu n'es pas encore enregistré comme participant ${user.displayName}, dépêche-toi de le faire si tu veux avoir une chance de gagner le giveaway !`)
			}
		}
	}

	relaunch(client, target) {
		let test = guardianDAO.getLatestParticipants()
	}

	instantiateHealthBar(guardian: any) {
		sse.updateHealthBar(guardian.health, guardian.currentHealth)
	}

	register(user: User) {
		if(guardianDAO.isAlreadyParticipant(user)) {
			guardianDAO.addParticipant(user)
		}
	}
}
