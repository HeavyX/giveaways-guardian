var COMMANDS = {
	"kick" : {
		"dead": "Le gardien à été tué !! Attendez que IHeavyX en trouve un nouveau à affronter !"
	},
	"guardian": {
		"fighting": "Vous affrontez actuellement",
		"stillHas": "et il lui reste encore",
		"healthPoints" : "points de vie !"
	},
	"bitch": [
		"༼ง=ಠ益ಠ=༽ง",
		"(ง •̀_•́)ง",
		"( ͠° ͟ʖ ͡°)",
		"(╯°□°)╯︵ ┻━┻",
		"⎝ PogChamp ⎠",
		"(ง ͠° ل͜ °)ง",
		"FBPass cmonBruh FBBlock"
	],
	"giveaway" : {
		"noWinner" : "Aucun gagnant ?",
		"winnerIs" : "AND THE WINNER IS",
		"congratulation" : "!! Félicitations !!",
		"notDefeated" : "Hum Hum... ne nous enflammons pas, le Gardien du Giveaway n'est pas encore vaincu..."
	},
	"win" : {
		"t" : ""
	},
	"stats" : {
		"hit" : "coup",
		"hits" : "coups"
	}
}
export default COMMANDS
