import { LowDBHandler } from './lowdb.utilities'
import * as _ from 'lodash'

export class GuardianDAO {
	private _lowdb: LowDBHandler

	constructor() {
		this.lowdb = new LowDBHandler(`./src/db/guardians.json`, {count: 0, guardians: [], actualGuardianId: 0})
	}

	getCurrentId() {
		return this.lowdb.get('actualGuardianId')
	}

	updateCurrentId(id) {
		this.lowdb.updateField('actualGuardianId', id)
	}

	resetCurrent() {
		return this.lowdb.updateField('actualGuardianId', -1)
	}

	getById(guardianId) {
		return this.lowdb.getInArray('guardians', {id: guardianId})
	}

	push(id, name, health, currentHealth, participants) {
		return this.lowdb.push('guardians', {
			id: id,
			name: name,
			health: health,
			currentHealth: currentHealth,
			participants: participants
		})
	}

	updateHealth(currentHealth) {
		const guardianId = this.getCurrentId()
		return this.lowdb.updateArray('guardians', {id: guardianId}, {currentHealth: currentHealth})
	}

	getCount() {
		return this.lowdb.get('count')
	}

	incrementCount() {
		return this.lowdb.increment('count')
	}

	removeHealthWin(currentHealth) {
		const guardianId = this.getCurrentId()
		return this.lowdb.updateArray('guardians', {id: guardianId}, {currentHealth: currentHealth})
	}

	addParticipant(user) {
		const guardianId = this.getCurrentId()
		return this.lowdb.pushInNestedArray('guardians', {id: guardianId}, 'participants', {user: user.displayName, damageDealt: 0, numberOfHits: 0})
	}

	isAlreadyParticipant(user) {
		const guardianId = this.getCurrentId()
		return !!this.lowdb.getObjectInNestedArray('guardians', {id: guardianId}, 'participants', {user: user.displayName})
	}

	getParticipants(id: number) {
		return this.lowdb.getInNestedArray('guardians', {id: id}, 'participants')
	}

	getCurrentParticipants() {
		return this.getParticipants(this.getCurrentId())
	}

	getLatestParticipants() {
		let test = this._lowdb.get('guardians') // qziyrgqzçh orhuqçpiquohr pqizur can't do it
		return test
	}

	updateDealtDamages(damageDealt, user) {
		const guardianId = this.getCurrentId()
		this.lowdb.incrementObjectInNestedArray('guardians', {id: guardianId}, 'participants', {user: user.displayName}, 'damageDealt', damageDealt)
	}

	updateNumberOfHits(user) {
		const guardianId = this.getCurrentId()
		this.lowdb.incrementObjectInNestedArray('guardians', {id: guardianId}, 'participants', {user: user.displayName}, 'numberOfHits', 1)
	}

	hasGuardianInProgress() {
		return this.lowdb.get('actualGuardianId') > 0
	}

	getCurrentGuardian() {
		const guardianId = this.getCurrentId()
		return this.lowdb.getInArray('guardians', {id: guardianId})
	}

	getParticipant(user) {
		const guardianId = this.getCurrentId()
		return this.lowdb.getObjectInNestedArray('guardians', {id: guardianId}, 'participants', {user: user.displayName})
	}

	/* GETTERS & SETTERS */
	get lowdb() {
		return this._lowdb
	}
	set lowdb(lowdb) {
		this._lowdb = lowdb
	}
}
