import * as FileSync from 'lowdb/adapters/FileSync'
import * as lowdb from 'lowdb'

export class LowDBHandler {
	private _adapter: FileSync
	private _db: lowdb

	constructor(dbPath, dbStructure = null) {
		this.adapter = new FileSync(dbPath)
		this.db = new lowdb(this.adapter)

		if(dbStructure) this.db.defaults(dbStructure).write()
	}

	/*============================= NON NESTED =============================*/

	/* FIELDS */
	get(fieldPath) {
		this.refreshDB()
		return this.db.get(fieldPath).value()
	}

	set(fieldPath, value) {
		this.refreshDB()
		return this.db.set(fieldPath, value).write()
	}

	updateField(fieldPath, value) {
		this.refreshDB()
		return this.db.update(fieldPath, n => value).write()
	}

	remove(fieldPath, value) {
		this.refreshDB()
		return this.db.get(fieldPath).remove(value).write()
	}


	/* ARRAYS */
	getInArray(fieldPath, filter) {
		this.refreshDB()
		return this.db.get(fieldPath).find(filter).value()
	}

	updateArray(fieldPath, filter, value) {
		this.refreshDB()
		return this.db.get(fieldPath).filter(filter).find(filter).assign(value).write()
	}

	push(fieldPath, value) {
		this.refreshDB()
		return this.db.get(fieldPath).push(value).write()
	}

	/* SPECIFIC */
	increment(value) {
		this.refreshDB()
		return this.db.update(value, n => n + 1).write()
	}


	/*============================= NESTED =============================*/

	pushInNestedArray(fieldPath, filter, arrayPath, value) {
		this.refreshDB()
		return this.db.get(fieldPath).find(filter).get(arrayPath).push(value).write()
	}

	getInNestedArray(fieldPath, filter, arrayPath) {
		this.refreshDB()
		return this.db.get(fieldPath).find(filter).get(arrayPath).value()
	}

	updateInNestedArray(fieldPath, filter, arrayPath, arrayFilter, value) {
		this.refreshDB()
		return this.db.get(fieldPath).find(filter).get(arrayPath).find(arrayFilter).assign(value).value()
	}

	getObjectInNestedArray(fieldPath, filter, arrayPath, arrayFilter) {
		this.refreshDB()
		return this.db.get(fieldPath).find(filter).get(arrayPath).find(arrayFilter).value()
	}

	updateObjectInNestedArray(fieldPath, filter, arrayPath, arrayFilter, valueFilter, value) {
		this.refreshDB()
		return this.db.get(fieldPath).find(filter).get(arrayPath).find(arrayFilter).update(valueFilter, n => value).write()
	}

	/* SPECIFIC */
	incrementObjectInNestedArray(fieldPath, filter, arrayPath, arrayFilter, valueFilter, value) {
		this.refreshDB()
		return this.db.get(fieldPath).find(filter).get(arrayPath).find(arrayFilter).update(valueFilter, n => n + value).write()
	}

	
	/* DO NOT TOUCH THESE */
	removeProperty(fieldPath) {
		this.refreshDB()
		return this.db.unset(fieldPath).write()
	}

	getEntireDB() {
		this.refreshDB()
		return this.db.getState()
	}

	setEntireDB(value) {
		this.refreshDB()
		return this.db.setState(value)
	}

	refreshDB() {
		this.db.read()
	}
	
	/* GETTERS & SETTERS */
	get adapter() {
		return this._adapter
	}
	set adapter(adapter) {
		this._adapter = adapter
	}

	get db() {
		return this._db
	}
	set db(db) {
		this._db = db
	}
}