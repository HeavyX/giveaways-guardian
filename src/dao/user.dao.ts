import { LowDBHandler } from './lowdb.utilities'
import * as Moment from 'moment'

export class UserDAO {
	private _lowdb: LowDBHandler

  constructor() {
    this.lowdb = new LowDBHandler(`./src/db/users.json`, {users: [], bot: []})
	}
	
	getCommandCooldown(userId, commandName) {
		return this.lowdb.getObjectInNestedArray('users', {userId: userId}, 'commandsCooldown', {command: commandName})
	}

	getBotCommandCooldown(commandName) {
		return this.lowdb.getInArray('bot', {command: commandName})
	}

	getUserById(userId) {
		return this.lowdb.getInArray('users', {userId: userId})
	}

	push(username, displayName, isModerateur, isSubscribed, foughtGuardians, commandsCooldown, userId) {
    return this.lowdb.push('users', {
			username: username, 
			displayName: displayName, 
			isModerateur: isModerateur, 
			isSubscribed: isSubscribed,
			foughtGuardians: foughtGuardians,
			commandsCooldown: commandsCooldown,
			userId: userId
		})
	}
	
	getFoughtGuardiansList(userId) {
		return this.lowdb.getInNestedArray('users', {userId: userId}, 'foughtGuardians')
	}

	pushFoughtGuardian(userId, guardianName) {
		return this.lowdb.pushInNestedArray('users', {userId: userId}, 'foughtGuardians', guardianName)
	}

	initializeCommandCooldown(userId, commandName) {
		return this.lowdb.pushInNestedArray('users', {userId: userId}, 'commandsCooldown', {command: commandName, lastUsage: Moment()})
	}

	updateCommandCooldown(userId, commandName) {
		return this.lowdb.updateObjectInNestedArray('users', {userId: userId}, 'commandsCooldown', {command: commandName}, 'lastUsage', Moment())
	}

	canUseBotCommand(commandName, cooldown) {
		const botCommandCooldown = this.getBotCommandCooldown(commandName)
		if(!botCommandCooldown) return true
		const date = Moment(botCommandCooldown.lastUsage)
		return parseInt(Moment(Moment().diff(date)).format("m")) >= cooldown
	}

	updateBotLastUsage(commandName) {
		const botCommandCooldown = this.getBotCommandCooldown(commandName)
		if(!botCommandCooldown) this.initializeBotCommandCooldown(commandName)
		else this.updateBotCommandCooldown(commandName)
	}

	updateBotCommandCooldown(commandName) {
		return this.lowdb.updateArray('bot', {command: commandName}, {lastUsage: Moment()})
	}

	initializeBotCommandCooldown(commandName) {
		return this.lowdb.push('bot', {command: commandName, lastUsage: Moment()})
	}

	/* GETTERS & SETTERS */
  get lowdb() {
		return this._lowdb
	}
	set lowdb(lowdb) {
		this._lowdb = lowdb
	}
}