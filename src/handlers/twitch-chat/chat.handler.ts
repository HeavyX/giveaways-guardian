import { PermissionHandler } from '../../services/permission.service';
import { CommandsService } from '../../services/commands.service';
import { Guardian } from '../../models/guardian.model';
import { Commands } from '../../commands/commands';
import { User } from '../../models/user.model';
import * as tmi from 'tmi.js'
import TEXTS from '../../config/texts';
import { GuardianDAO } from '../../dao/guardian.dao';

export class ChatHandler {
	private _client: tmi.Client
	private _guardian: Guardian
	private _guardianDAO: GuardianDAO
	private _commands: Commands
	private _commandsService: CommandsService

	constructor(options) {
		this.client = new tmi.Client(options)
		this.guardian = null
		this.guardianDAO = new GuardianDAO()
		this.commands = new Commands()
		this.commandsService = new CommandsService()
		this.listenToConnection()
		this.listenToChat(this)
	}

	// launches the connection via tmi.js library
	connect() {
		this.client.connect()
		this.resumeGuardian()
	}

	// announces that we are connected to the irc chat
	listenToConnection() {
		this.client.on('connected', (addr, port) => {
			console.info(`* Connected to ${addr}:${port}`);
		})
	}

	// core of the program, calls the commands
	listenToChat(instance: any) {
		this.client.on('message', (target: any, userContext: any, msg: any, self: any) => {
			if(self) return
			if(!instance.commandsService.isValidCommand(msg, userContext)) return

			const user = new User(userContext)
			user.insertUser()

			const commandName = instance.commandsService.getCommandName(msg, userContext)
			const commandParams = instance.commandsService.getCommandParams(msg)

			if(!instance.guardian && commandName !== 'instantiate' && commandName !== 'bitch') { // In case no guardian is set
				instance.client.say(target, TEXTS.noGuardian)
				return
			}

			switch(commandName) {
				// PLEB COMMANDS
				case "register": {
					instance.commands.register(user)
					break;
				}
				case "kick": {
					instance.commands.kick(user, instance.guardian, instance.client, target)
					break;
				}
				case "guardian": {
					instance.commands.guardian(instance.guardian, instance.client, target)
					break;
				}
				case "stats": {
					instance.commands.stats(user, instance.guardian, instance.client, target)
					break;
				}
				case "bitch": {
					instance.commands.bitch(instance.client, target)
					break;
				}

				// ADMIN COMMANDS
				case "instantiate": {
					instantiateBot()
					break;
				}
				case "giveaway": {
					if(isBroadcaster()) instance.commands.giveaway(instance.guardian, instance.client, target)
					break;
				}
				case "relaunch": {
					if(isBroadcaster()) instance.commands.relaunch(instance.client, target)
					break;
				}
				case "win": {
					if(isBroadcaster()) instance.commands.win(commandParams, instance.guardian, instance.client, target)
					break;
				}
			}

			function isBroadcaster() { // PROPOSITION: In the User model to use the user variable ?
				return PermissionHandler.hasPermission(userContext, 5)
			}

			function instantiateBot() {
				if(isBroadcaster()) {
					if(!instance.guardianDAO.hasGuardianInProgress()) {
						instance.guardian = new Guardian(parseInt(commandParams[0]), parseInt(commandParams[0]), commandParams[1])
						instance.guardian.insertGuardian()
						instance.commands.instantiateHealthBar(instance.guardian)
						instance.client.say(target, TEXTS.newGuardian.found + " " + TEXTS.newGuardian.name + " " + instance.guardian.name + " " + TEXTS.newGuardian.andHas + " " + instance.guardian.health + " " + TEXTS.newGuardian.healthPoints)
					} else {
						instance.client.say(target, TEXTS.alreadyFighthting)
					}
				}
			}
		console.info(`* Executed ${commandName} command`)
		})
	}

	resumeGuardian() {
		if(this.guardianDAO.hasGuardianInProgress()) {
			const newGuardian = this.guardianDAO.getCurrentGuardian()
			this.guardian = new Guardian(newGuardian.health, newGuardian.currentHealth, newGuardian.name)
			this.commands.instantiateHealthBar(this.guardian)
		}
	}

	/* GETTERS & SETTERS */
	get client() {
		return this._client
	}
	set client(client) {
		this._client = client
	}

	get guardian() {
		return this._guardian
	}
	set guardian(guardian) {
		this._guardian = guardian
	}

	get guardianDAO() {
		return this._guardianDAO
	}
	set guardianDAO(guardianDAO) {
		this._guardianDAO = guardianDAO
	}

	get commands() {
		return this._commands
	}
	set commands(commands) {
		this._commands = commands
	}

	get commandsService() {
		return this._commandsService
	}
	set commandsService(commandsService) {
		this._commandsService = commandsService
	}
}
