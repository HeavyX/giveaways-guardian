import { GuardianDAO } from '../dao/guardian.dao';
import { User } from './user.model';

export class Guardian {
	private _name: string = "IHeavyX"
	private _health: number = 2000
	private _currentHealth: number = 2000
	private _participants: any[]
	private _guardianDAO: GuardianDAO

	constructor(health: number, currentHealth: number, name: string, participants = []) {
		if (!isNaN(health)) {
			this.name = name
			this.health = health
			this.currentHealth = currentHealth
		}
		this.participants = participants
		this.guardianDAO = new GuardianDAO()
	}

	isDead() {
		return this.currentHealth <= 0
	}

	removeHealth(damageDealt: number, user: User) {
		this.currentHealth -= damageDealt
		this.updateActualGuardian(damageDealt, user)
	}

	removeHealthWin(damageDealt: number) {
		this.currentHealth -= damageDealt
		this.guardianDAO.removeHealthWin(this.currentHealth)
	}

	updateActualGuardian(damageDealt: number, user: User) {
		this.guardianDAO.updateHealth(this.currentHealth)
		this.guardianDAO.updateDealtDamages(damageDealt, user)
		this.guardianDAO.updateNumberOfHits(user)
	}

	insertGuardian() {
		const id = parseInt(this.guardianDAO.getCount()) + 1
		this.guardianDAO.push(id, this.name, this.health, this.currentHealth, this.participants)
		this.guardianDAO.updateCurrentId(id)
		this.guardianDAO.incrementCount()
	}

	/* MOVE IN USER DAO */
	getUserStats(user: User) {
		const participant = this.guardianDAO.getParticipant(user)
		if (participant) return participant
	}

	giveaway() {
		const participants = this.guardianDAO.getCurrentParticipants()
		this.guardianDAO.resetCurrent()
		return participants[Math.floor(Math.random() * participants.length)]
	}

	/* GETTERS & SETTERS */
	get name() {
		return this._name
	}
	set name(name) {
		this._name = name
	}

	get health() {
		return this._health
	}
	set health(health) {
		this._health = health
	}

	get currentHealth() {
		return this._currentHealth
	}
	set currentHealth(currentHealth) {
		this._currentHealth = currentHealth
	}

	get participants() {
		return this._participants
	}
	set participants(participants) {
		this._participants = participants
	}

	get guardianDAO() {
		return this._guardianDAO
	}
	set guardianDAO(guardianDAO) {
		this._guardianDAO = guardianDAO
	}
}
