import { LowDBHandler } from '../dao/lowdb.utilities'
import * as Moment from 'moment'
import { UserDAO } from '../dao/user.dao';

export class User {
	private _username: string
	private _displayName: string
	private _isModerateur: boolean
	private _isSubscribed: boolean
	private _userId: number
	private _foughtGuardians: any[]
	private _commandsCooldown: any[]
	private _userDAO: UserDAO

	constructor(userContext: any, foughtGuardians = [], commandsCooldown = []) {
		this.username = userContext.username
		this.displayName = userContext['display-name']
		this.isModerateur = userContext.mod
		this.isSubscribed = userContext.subscriber
		this.userId = userContext['user-id']
		this.foughtGuardians = foughtGuardians
		this.commandsCooldown = commandsCooldown
		this.userDAO = new UserDAO()
	}

	canUseCommand(commandName, cooldown) {
		const commandCooldown = this.userDAO.getCommandCooldown(this.userId, commandName)
		if(!commandCooldown) return true
		const date = Moment(commandCooldown.lastUsage)
		return parseInt(Moment(Moment().diff(date)).format("m")) >= cooldown
	}

	getRemainingTime(commandName) {
		const commandCooldown = this.userDAO.getCommandCooldown(this.userId, commandName)
		const endDate = Moment(commandCooldown.lastUsage).add(5, 'minutes')
		return Moment(endDate.diff(Moment())).format("m[m] s[s]")
	}

	insertUser() {
		if(!this.userDAO.getUserById(this.userId)) {
			this.userDAO.push(this.username, this.displayName, this.isModerateur, this.isSubscribed, this.foughtGuardians, this.commandsCooldown, this.userId)
		}
	}

	addParticipation(guardian) {
		const foughtGuardiansList = this.userDAO.getFoughtGuardiansList(this.userId) // Think about pushing the id of the guardian as well, name isn't a good identifier
		if(foughtGuardiansList.indexOf(guardian.name) == -1) {
			this.userDAO.pushFoughtGuardian(this.userId, guardian.name)
		}
	}

	updateUserLastUsage(commandName) {
		const commandCooldown = this.userDAO.getCommandCooldown(this.userId, commandName)
		if(!commandCooldown) this.userDAO.initializeCommandCooldown(this.userId, commandName)
		else this.userDAO.updateCommandCooldown(this.userId, commandName)
	}

	rollDice() {
		const sides = 6;
		return Math.floor(Math.random() * sides) + 1;
	}

	/* GETTERS & SETTERS */
	get username() {
		return this._username
	}
	set username(username) {
		this._username = username
	}

	get displayName() {
		return this._displayName
	}
	set displayName(displayName) {
		this._displayName = displayName
	}

	get isModerateur() {
		return this._isModerateur
	}
	set isModerateur(isModerateur) {
		this._isModerateur = isModerateur
	}

	get isSubscribed() {
		return this._isSubscribed
	}
	set isSubscribed(isSubscribed) {
		this._isSubscribed = isSubscribed
	}

	get userId() {
		return this._userId
	}
	set userId(userId) {
		this._userId = userId
	}

	get foughtGuardians() {
		return this._foughtGuardians
	}
	set foughtGuardians(foughtGuardians) {
		this._foughtGuardians = foughtGuardians
	}

	get commandsCooldown() {
		return this._commandsCooldown
	}
	set commandsCooldown(commandsCooldown) {
		this._commandsCooldown = commandsCooldown
	}

	get userDAO() {
		return this._userDAO
	}
	set userDAO(userDAO) {
		this._userDAO = userDAO
	}
}
