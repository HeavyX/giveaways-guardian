export class CommandsService {
	isValidCommand(message, userContext) {
		return (message.startsWith("!") && this.checkWhiteSpaces(message)) || this.isChannelPointsCommand(userContext)
	}

	isValidCommandName(commandName) {
		return /^[a-zA-Z0-9]+$/.test(commandName);
	}

	isValidCommandContent(content) {
		return !(content[0].startsWith("/") || content[0].startsWith(".")) || content[0].startsWith('!');
	}

	getCommandName(rawInput, userContext) {
		if (this.isRegisterCommand(userContext)) return 'register'
		let input = this.unwrapCommandPrefix(rawInput);
			return input.split(' ')[0];
	}

	getCommandParams(rawInput) {
		let input = this.unwrapCommandPrefix(rawInput);
		let splitStr = input.split(' ');
		return splitStr.slice(1, splitStr.length);
	}

	checkWhiteSpaces(input) {
		return /\S/.test(input.slice(1, input.length)) && input.charAt(1) !== ' ';
	}

	unwrapCommandPrefix(rawInput) {
		return rawInput.substr(1);
	}

	isChannelPointsCommand(userContext) {
		return userContext['custom-reward-id']
	}

	isRegisterCommand(userContext) {
		return userContext['custom-reward-id'] && userContext['custom-reward-id'] == 'ec7b285a-eb52-4967-9c45-5046e9b0922f'
	}
}
