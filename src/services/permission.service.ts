export class PermissionHandler {
	static hasPermission(userContext: any, requiredPermission: number) {
		let permissionLevel: number

		if(userContext == null) return // Because accessing a property if the person doesn't have a badge might kill the bot
		if(userContext.badges.broadcaster) permissionLevel = 5
		else if(userContext.badges.moderator) permissionLevel = 3

		return permissionLevel >= requiredPermission
	}
}
